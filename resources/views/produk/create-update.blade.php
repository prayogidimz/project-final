@extends('layout.master')
@section('content')
<div class="main-panel">
    <div class="content-wrapper">
      <div class="row" id="proBanner">
      </div>
      <div class="d-xl-flex justify-content-between align-items-start">
        <h2 class="text-dark font-weight-bold mb-2"> {{ isset($data) ? 'Edit' : 'Tambah' }} Produk </h2>
      </div>
      <div class="row">
        <div class="col-md-12">
          <div class="tab-content tab-transparent-content">
            <div class="tab-pane fade show active" id="business-1" role="tabpanel" aria-labelledby="business-tab">
              <div class="row">
                <div class="col-12 grid-margin">
                  <div class="card">
                    <div class="card-body">
                      <div class="row">

                        <!-- content -->
                        <div class="col-md-12">
                            <div class="card-body">
                                <form class="forms-sample" action="{{ isset($data) ? route('produk.update', $data->id) : route('produk.store') }}" method="post" enctype="multipart/form-data">
                                    @csrf
                                    <div class="form-group">
                                        <label for="namaproduk">Nama Produk</label>
                                        <input type="text" class="form-control" placeholder="Nama Produk" name="nama_produk" value="{{ old('nama_produk', ($data->nama_produk ?? '')) }}">
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Kategori</label>
                                        <select name="kategori_id" class="form-control">
                                            <option value="">Pilih Kategori</option>
                                            @foreach ($kategori as $key => $s)
                                                <option value="{{ $s->id }}" {{ old('kategori', ($data->kategori_id ?? '')) == $s->id ? 'selected' : '' }}>{{ $s->nama }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label>Harga Produk</label>
                                        <input type="text" class="form-control" placeholder="Harga Produk" name="harga_produk" value="{{ old('harga_produk', ($data->harga_produk ?? '')) }}">
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputPassword1">Deskripsi</label>
                                        <textarea cols="30" rows="10" class="form-control" name="deskripsi">{{ old('deskripsi', ($data->deskripsi ?? '')) }}</textarea>
                                    </div>
                                    @if(isset($data))
                                    <div class="form-group">
                                        <img src="{{asset('/foto_produk/'.$data->foto_produk)}}" width="20%">
                                    </div>
                                    @endif
                                    <div class="form-group">
                                        <label for="exampleInputConfirmPassword1">Foto Produk</label>
                                        <input type="file" class="form-control" name="foto_produk">
                                    </div>
    
                                    <button type="submit" class="btn btn-primary mr-2">
                                        <span><i class="fa fa-save"></i></span> 
                                        Simpan
                                    </button>
                                    <a class="btn btn-light" >
                                        <span>
                                            <i class="fa fa-arrow-left"></i>
                                        </span>    
                                        Kembali
                                    </a>
                                </form>
                            </div>
                        </div>
                         <!-- content -->

                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    @push('script')
    <script>
        $(function() {
            $("#tab").DataTable({
                fixedHeader:true,
                scrollX:true
            });
        })
    </script>
     @endpush
@endsection