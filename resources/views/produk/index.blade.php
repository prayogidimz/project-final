@extends('layout.master')
@section('content')
<div class="main-panel">
    <div class="content-wrapper">
      <div class="row" id="proBanner">
      </div>
      <div class="d-xl-flex justify-content-between align-items-start">
        <h2 class="text-dark font-weight-bold mb-2"> Produk </h2>
      </div>
      <div class="row">
        <div class="col-md-12">
          <div class="tab-content tab-transparent-content">
            <div class="tab-pane fade show active" id="business-1" role="tabpanel" aria-labelledby="business-tab">
              <div class="row">
                <div class="col-12 grid-margin">
                  <div class="card">
                    <div class="card-body">
                      <div class="row">
                        <div class="col-sm-12">
                          <div class="d-flex justify-content-between align-items-center mb-4">
                            <a href="{{ route('produk.create') }}" class="btn btn-primary menu-title">
                                <span>
                                    <i class="fa fa-plus"> Tambah</i>
                                </span>
                            </a>
                          </div>
                        </div>

                        <!-- content -->
                        <div class="col-md-12">
                            <table class="table table-striped" id="tab">
                                <thead>
                                    <tr>
                                        <th>No.</th>
                                        <th>Nama Produk</th>
                                        <th>Harga Produk</th>
                                        <th>Kategori</th>
                                        <th>Deskripsi</th>
                                        <th>Foto Produk</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($data as $key => $d)
                                        <tr>
                                            <td>{{ $key + 1 }}.</td>
                                            <td>{{ $d->nama_produk }}</td>
                                            <td>{{ $d->harga_produk }}</td>
                                            <td>{{ $d->getKategori->nama ?? '-'}}</td>
                                            <td>{{ $d->deskripsi }}</td>
                                            <td>
                                                <img src="{{asset('/foto_produk/'.$d->foto_produk)}}" onerror="this.onerror=null;this.src='images/camera.png';" alt="">
                                            </td>
                                            <td>
                                                <a href="{{ route('produk.edit',$d->id ) }}" class="btn btn-warning btn-sm">
                                                    <span><i class="fa fa-pencil"></i></span>
                                                </a>
                                                <a href="{{ route('produk.destroy',$d->id ) }}" class="btn btn-danger btn-sm">
                                                    <span><i class="fa fa-trash"></i></span>
                                                </a>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                         <!-- content -->

                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    @push('script')
    <script>
       $(document).ready(function () {
            $('#tab').DataTable();
        });
    </script>
     @endpush
@endsection