@extends('layout.master')
@section('content')
<div class="main-panel">
    <div class="content-wrapper">
      <div class="row" id="proBanner">
      </div>
      <div class="d-xl-flex justify-content-between align-items-start">
        <h2 class="text-dark font-weight-bold mb-2"> Pemesanan </h2>
      </div>
      <div class="row">
        <div class="col-md-12">
          <div class="tab-content tab-transparent-content">
            <div class="tab-pane fade show active" id="business-1" role="tabpanel" aria-labelledby="business-tab">
              <div class="row">
                <div class="col-12 grid-margin">
                  <div class="card">
                    <div class="card-body">
                      <div class="row">
                        <div class="col-sm-12">
                          <div class="d-flex justify-content-between align-items-center mb-4">
                          </div>
                        </div>

                        <!-- content -->
                        <div class="col-md-12">
                            <table class="table table-striped" id="tab">
                                <thead>
                                    <tr>
                                        <th>No.</th>
                                        <th>Nama Konsumen</th>
                                        <th>Nama Produk</th>
                                        <th>Total Beli</th>
                                        <th>Total Harga</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($data as $key => $d)
                                        <tr>
                                            <td>{{ $key + 1 }}.</td>
                                            <td>{{ $d->konsumen->nama ?? null}}</td>
                                            <td>{{ $d->produk->nama_produk ?? null}}</td>
                                            <td>{{ $d->jumlah_beli }}</td>
                                            <td>{{ $d->jumlah_harga }}</td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                         <!-- content -->

                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    @push('script')
    <script>
       $(document).ready(function () {
            $('#tab').DataTable();
        });
    </script>
     @endpush
@endsection