@extends('layout.master')
@section('content')
<div class="main-panel">
    <div class="content-wrapper">
      <div class="row" id="proBanner">
      </div>
      <div class="d-xl-flex justify-content-between align-items-start">
        <h2 class="text-dark font-weight-bold mb-2"> {{ isset($data) ? 'Edit' : 'Tambah' }} Kategori </h2>
      </div>
      <div class="row">
        <div class="col-md-12">
          <div class="tab-content tab-transparent-content">
            <div class="tab-pane fade show active" id="business-1" role="tabpanel" aria-labelledby="business-tab">
              <div class="row">
                <div class="col-12 grid-margin">
                  <div class="card">
                    <div class="card-body">
                      <div class="row">

                        <!-- content -->
                        <div class="col-md-12">
                            <div class="card-body">
                                <form class="forms-sample" action="{{ isset($data) ? route('kategori.update', $data->id) : route('kategori.store') }}" method="post" enctype="multipart/form-data">
                                    @csrf
                                    <div class="form-group">
                                        <label for="namakategori">Nama Kategori</label>
                                        <input type="text" class="form-control" placeholder="Nama Kategori" name="nama" value="{{ old('nama', ($data->nama ?? '')) }}">
                                    </div>
    
                                    <button type="submit" class="btn btn-primary mr-2">
                                        <span><i class="fa fa-save"></i></span> 
                                        Simpan
                                    </button>
                                    <a class="btn btn-light" >
                                        <span>
                                            <i class="fa fa-arrow-left"></i>
                                        </span>    
                                        Kembali
                                    </a>
                                </form>
                            </div>
                        </div>
                         <!-- content -->

                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    @push('script')
    <script>
        $(function() {
            $("#tab").DataTable({
                fixedHeader:true,
                scrollX:true
            });
        })
    </script>
     @endpush
@endsection