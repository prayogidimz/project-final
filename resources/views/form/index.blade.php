@extends('layout.masterfe')

@section('content')

          <div class="container">
            <div class="billing_details">
              <div class="row">
                <div class="col-lg-12">
                  <h3>Form Data Diri</h3>
                  <form class="row contact_form" action="{{ route('form.store') }}" method="post" enctype="multipart/form-data">
                    @csrf
                    <div class="col-md-12 form-group">
                      <input type="text" class="form-control" id="nama" name="nama" placeholder="nama" />
                    </div>
                    <div class="col-md-6 form-group p_star">
                      <input type="text" class="form-control" id="number" name="telp" />
                    </div>
                    <div class="col-md-6 form-group p_star">
                      <input type="text" class="form-control" id="email" name="email" />
                    </div>
                    <div class="col-md-12 form-group">
                      <textarea class="form-control" name="alamat" id="message" rows="1"
                        placeholder="Alamat"></textarea>
                    </div>
                    <div class="col-md-12 form-group">
                        <button type="submit" class="btn btn-primary">Simpan</button>
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
        
@endsection