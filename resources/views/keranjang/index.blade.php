@extends('layout.masterfe')

@section('content')
      <div class="container">
        <h3>Cart</h3>
          <div class="cart_inner">
            <div class="table-responsive">
              <table class="table">
                <thead>
                  <tr>
                    <th scope="col">Product</th>
                    <th scope="col">Price</th>
                    <th scope="col">Quantity</th>
                    <th scope="col">Total</th>
                  </tr>
                </thead>
                <tbody>
                  @php $jumlah = 0; @endphp
                  @if(session('cart') != null)
                  @foreach(session('cart') as $id => $produk)
                  <tr>
                    <td>
                      <div class="media">
                        <div class="d-flex">
                          <img src="assets/img/gallery/card1.png" alt="" />
                        </div>
                        <div class="media-body">
                          <p>{{ $produk['nama_produk' ]}}</p>
                        </div>
                      </div>
                    </td>
                    <td>
                      <h5>{{ number_format($produk['quantity'])}}</h5>
                    </td>
                    <td>
                        {{ $produk['quantity']}}
                    </td>
                    <td>
                      @php 
                        $hasil = $produk['harga_produk'] *$produk['quantity'];
                        $jumlah += $hasil;
                      @endphp
                      <h5>{{number_format($hasil)}}</h5>
                    </td>
                  </tr>
                  @endforeach
                  @endif
                  <tr>
                    <td></td>
                    <td></td>
                    <td>
                      <h5>Subtotal</h5>
                    </td>
                    <td>
                      <h5>{{number_format($jumlah)}}</h5>
                    </td>
                  </tr>
                </tbody>
              </table>
              <div class="checkout_btn_inner float-right">
                <a class="btn_1" href="{{ route('home.index') }}">Continue Shopping</a>
                <a class="btn_1 checkout_btn_1" href="{{route('checkout')}}">Proceed to checkout</a>
              </div>
            </div>

@endsection