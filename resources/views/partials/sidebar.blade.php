<nav class="sidebar sidebar-offcanvas" id="sidebar">
    <ul class="nav">
      <li class="nav-item nav-category">Main</li>
      <li class="nav-item">
        <a class="nav-link" href="{{ route('dashboard') }}">
          <span class="icon-bg"><i class="mdi mdi-cube menu-icon"></i></span>
          <span class="menu-title">Dashboard</span>
        </a>
      </li>
      <li class="nav-item">
        <a class="nav-link" data-toggle="collapse" href="#produk" aria-expanded="false" aria-controls="produk">
          <span class="icon-bg"><i class="mdi mdi-lock menu-icon"></i></span>
          <span class="menu-title">Produk</span>
          <i class="menu-arrow"></i>
        </a>
        <div class="collapse" id="produk">
          <ul class="nav flex-column sub-menu">
            <li class="nav-item"> <a class="nav-link" href="{{ route('produk.index') }}"> List Produk </a></li>
            <li class="nav-item"> <a class="nav-link" href="{{ route('produk.create') }}"> Create Produk</a></li>
          </ul>
        </div>
      </li>
      <li class="nav-item">
        <a class="nav-link" data-toggle="collapse" href="#kategori" aria-expanded="false" aria-controls="kategori">
          <span class="icon-bg"><i class="mdi mdi-lock menu-icon"></i></span>
          <span class="menu-title">Kategori</span>
          <i class="menu-arrow"></i>
        </a>
        <div class="collapse" id="kategori">
          <ul class="nav flex-column sub-menu">
            <li class="nav-item"> <a class="nav-link" href="{{ route('kategori.index') }}"> List Kategori </a></li>
            <li class="nav-item"> <a class="nav-link" href="{{ route('kategori.create') }}"> Create Kategori</a></li>
          </ul>
        </div>
      </li>
      <li class="nav-item">
        <a class="nav-link" data-toggle="collapse" href="#pemesanan" aria-expanded="false" aria-controls="pemesanan">
          <span class="icon-bg"><i class="mdi mdi-lock menu-icon"></i></span>
          <span class="menu-title">pemesanan</span>
          <i class="menu-arrow"></i>
        </a>
        <div class="collapse" id="pemesanan">
          <ul class="nav flex-column sub-menu">
            <li class="nav-item"> <a class="nav-link" href="{{ route('pemesanan.index') }}"> List Pemesanan </a></li>
          </ul>
        </div>
      </li>
      <li class="nav-item">
        <a class="nav-link" data-toggle="collapse" href="#konsumen" aria-expanded="false" aria-controls="konsumen">
          <span class="icon-bg"><i class="mdi mdi-lock menu-icon"></i></span>
          <span class="menu-title">Konsumen</span>
          <i class="menu-arrow"></i>
        </a>
        <div class="collapse" id="konsumen">
          <ul class="nav flex-column sub-menu">
            <li class="nav-item"> <a class="nav-link" href="{{ route('konsumen.index') }}"> List Konsumen </a></li>
          </ul>
        </div>
      </li>
    </ul>
  </nav>