<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Produk;

class KeranjangController extends Controller
{
    public function index(){
        return view('keranjang.index');
    }
    public function cart(Request $request, $id)
    {
        if (empty(session()->get('user_id'))) {
            return redirect()->route('form');
        }

        $produk = Produk::find($id);

            if (!$produk) {
                abort(404);
            }

            $cart = session()->get('cart');

            // Jika session cart belum ada, buatlah session baru
            if (!$cart) {
                $cart = [
                    $id => [
                        'id_produk' => $produk->id,
                        'nama_produk' => $produk->nama_produk,
                        'harga_produk' => $produk->harga_produk,
                        'quantity' => 1
                    ]
                ];

                session()->put('cart', $cart);

                return redirect()->to('keranjang')->with('success', 'Produk berhasil ditambahkan ke dalam keranjang belanja.');
            }

            // Jika session cart sudah ada, tambahkan produk ke dalam session
            if (isset($cart[$id])) {
                $cart[$id]['quantity']++;
            } else {
                $cart[$id] = [
                    'id_produk' => $produk->id,
                    'nama_produk' => $produk->nama_produk,
                    'harga_produk' => $produk->harga_produk,
                    'quantity' => 1,
                ];
            }

            session()->put('cart', $cart);

            return redirect()->to('keranjang')->with('success', 'Produk berhasil ditambahkan ke dalam keranjang belanja.');
    }
}
