<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Kategori;

class KategoriController extends Controller
{
    public function index(Request $request){
        $data = Kategori::where('status',0)
        ->where(function($query) use($request){
            if($request->get('kategori') != null){
                $query->whereDate('kategori', '>=', $request->get('kategori'));
            }
        })->get();
        
        return view('kategori.index', compact('data'));
    }

    public function create()
    {
        return view('kategori.create-update');
    }

    public function store(Request $request)
    {
        $request->validate([
            'nama'      => 'required',
        ]);
     
        $data = New Kategori();
        $data->nama  = $request->input('nama');
        $data->save();

        return redirect(route('kategori.index'))
                    ->with('success', 'Data berhasil disimpan');
    }

    public function edit($id)
    {
        $data       = Kategori::findOrFail($id);
    
        return view('kategori.create-update', compact('data'));
    }

    public function Update(Request $request,$id)
    {
        $request->validate([
            'nama'      => 'required',
        ]);
     
        $data = Kategori::find($id);
        $data->nama  = $request->input('nama');
        $data->save();

        return redirect(route('kategori.index'))
                    ->with('success', 'Data berhasil disimpan');
    }

    public function destroy($id)
    {
        $data           = Kategori::findOrFail($id);
        $data->status   = 1;
        $data->save();

        return redirect(route('kategori.index'))
                    ->with('success', 'Data berhasil dihapus');
    }

}
