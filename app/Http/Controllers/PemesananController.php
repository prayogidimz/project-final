<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Pemesanan;

class PemesananController extends Controller
{
    public function index(){
        $data = Pemesanan::get();

        return view('pemesanan.index', compact('data'));
    }
}
