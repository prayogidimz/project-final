<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Konsumen;

class FormController extends Controller
{
    public function index()
    {
        return view('form.index');
    }

    public function store(Request $request)
    {
        $data = New Konsumen();
        $data->nama = $request->input('nama');
        $data->alamat = $request->input('alamat');
        $data->email = $request->input('email');
        $data->telp = $request->input('telp');
        $data->save();

        //simpan session
        session()->put('user_id', $data->id);

        return redirect()->route('home.index');
    }
}
